public class IkanSpesial extends Ikan implements BisaTerbang{
    public IkanSpesial(String nama, String spesies, boolean isBeracun){
        super(nama, spesies, isBeracun);
    }

    @Override
    public String bersuara(){
        return ("Blub blub blub. Halo, Saya " + getNama() + ". Saya ikan yang " + beracun() + ". Saya juga bisa terbang lho!");
    }

    @Override
    public String bisaTerbang() {
        return ("NGEEEENGGGGG");
    }

    @Override
    public String toString(){
        return this.bersuara();
    }
}
    

