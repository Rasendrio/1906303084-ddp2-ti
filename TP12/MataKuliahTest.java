import static org.junit.Assert.*;
import org.junit.Test;

public class MataKuliahTest {
    @Test
    public void testNamaMatkul(){ 
        MataKuliah DDP2 = new MataKuliah("DDP2","CSGE601021");
        assertEquals(DDP2.getNama(), "DDP2");
    }

    @Test
    public void testKodeMatkul(){ 
        MataKuliah DDP2 = new MataKuliah("DDP2","CSGE601021");
        assertEquals(DDP2.getKode(), "CSGE601021");
    }

}
