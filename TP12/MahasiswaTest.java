import static org.junit.Assert.*;
import org.junit.Test;

public class MahasiswaTest {
    @Test
    public void testNamaMahasiswa(){ 
        Mahasiswa Burhan = new Mahasiswa("Burhan","129500004Y");
        assertEquals(Burhan.getNama(), "Burhan");
    }

    @Test
    public void testNPMMahasiswa(){ 
        Mahasiswa Burhan = new Mahasiswa("Burhan","129500004Y");
        assertEquals(Burhan.getNpm(), "129500004Y");
    }
}
