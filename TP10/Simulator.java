import java.util.Scanner;
import java.util.ArrayList;

public class Simulator {
    public static void main(String[] args){
        Manusia bujang = new Pegawai("Bujang", 100000.0, "pemula");
        Manusia yoga = new PegawaiSpesial("Yoga", 100000.0, "master");
        Hewan kerapu = new Ikan("Kerapu", "Epinephelus Polyphekadion", false); 
        Hewan ikanterbang = new IkanSpesial("Ikan Terbang Biru", "Exocoetus volitans", true);
        
        ArrayList<Manusia> manusia = new ArrayList<Manusia>();
        ArrayList<Hewan> hewan = new ArrayList<Hewan>();
        manusia.add(bujang);
        manusia.add(yoga);
        hewan.add(kerapu);
        hewan.add(ikanterbang);
        Scanner input = new Scanner(System.in);
        
        while (true){
            System.out.print("Masukkan perintah: ");
            String[] perintah = input.nextLine().toLowerCase().split(" ");
            if (perintah[0].equals("selesai")){
                break;
            }
            else if (perintah[0].equals("panggil")){
                try{
                    cari = cariManusia(manusia, perintah[1]);
                    cari = cariHewan(hewan, perintah[1]);
                    System.out.println(cari.toString());
                } catch (Exception e){
                    System.out.println(perintah[1] + " tidak bisa dipanggil");
                }
            }
        }
        input.close();
        System.out.println("Terimakasih dan Sampai Jumpa!");
    }
}

