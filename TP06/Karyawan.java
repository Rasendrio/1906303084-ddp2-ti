public class Karyawan{
    private String nama ;
    private int umur ;
    private int lamaBekerja ;

    public Karyawan(String nama, int umur, int lamaBekerja){
        this.nama = nama ;
        this.umur = umur ;
        this.lamaBekerja = lamaBekerja ;
    }

    public String getNama() {
        return this.nama ;
    }

    public int getUmur() {
        return this.umur ;
    }

    public int getLamaBekerja() {
        return this.lamaBekerja ;
    }

    public double getGaji() {
        double gaji = 100 ;
        int umur = getUmur() ;
        int lamaBekerja = getLamaBekerja() ;
        for (int i = 1 ; i <= lamaBekerja ; i++){
            if (i % 3 == 0 ){
                gaji += gaji * 5/100 ;
            }
        }
        if (umur > 40){
            gaji += 10 ;
        }
        return gaji ;
    }
}
