public abstract class Manusia implements Bernafas, Bergerak {
    private String nama;
    private Double uang;
    
    public Manusia(String nama, Double uang){
        this.nama = nama;
        this.uang = uang;
    }

    public String getNama(){
        return nama;

    }

    public Double getUang(){
        return uang;

    }

    public String bergerak(){
        return (this.getNama() + " bergerak dengan cara berjalan.");
    }
    public String bernafas(){
        return (this.getNama() + " bernafas dengan paru-paru.");
    }
    public String toString() {
        return this.bicara();
    }
}
