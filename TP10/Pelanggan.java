public class Pelanggan extends Manusia{

    public Pelanggan(String nama, Double uang){
        super(nama, uang);
    }

    public String beli() {
        return (getNama() + " membeli makanan dan minuman");
    }

    public String bicara() {
        return ("Halo, saya " + getNama() + ". Uang saya adalah " + getUang());
    }
}

