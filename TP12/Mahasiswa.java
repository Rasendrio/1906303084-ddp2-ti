import java.util.ArrayList;

public class Mahasiswa extends Manusia {
    private String nama;
    private String npm;
    private ArrayList<MataKuliah> daftarMatkul = new ArrayList<>();
    
    public Mahasiswa(String nama, String npm){
        this.nama = nama;
        this.npm = npm;
    }
    
    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getNpm() {
        return npm;
    }

    public void setNpm(String npm) {
        this.npm = npm;
    }

    public ArrayList<MataKuliah> getDaftarMatkul() {
        return daftarMatkul;
    }
    public void tambahMatkul(MataKuliah matkul) {
        this.daftarMatkul.add(matkul);
    }

    public void dropMatkul(MataKuliah matkul){
        try{
        daftarMatkul.remove(matkul);
        }
        catch(Exception ex) {
            System.out.println("");
        }
    }

    public String toString(){
        return "";
    }
}
