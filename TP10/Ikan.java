public class Ikan extends Hewan{
    private boolean isBeracun;

    public Ikan(String nama, String spesies, boolean isBeracun) {
        super(nama,spesies);
        this.isBeracun = isBeracun;
    }

    public String beracun(){
        if (this.isBeracun == true){
            return "beracun";
        } else {
            return "tidak beracun";
        }
    }

    public boolean getBeracun(){
        return isBeracun;
    } 

    public String bersuara(){
        return ("Blub blub blub. Halo, Saya " + getNama() + ". Saya ikan yang " + this.beracun());
    }

    public String bergerak(){
        return (getNama() + " bergerak dengan cara berenang.");
    }

    public String bernafas(){
        return (getNama() + " bernafas dengan insang.");
    }

    public String toString(){
        return this.bersuara();
    }
}
