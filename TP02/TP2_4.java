import java.util.Scanner ;

public class TP2_4 {
    public static void main(String[] args) {

        Scanner input = new Scanner(System.in) ;

        System.out.print("Masukkan sisi ab, ") ;
        System.out.print("bc,") ;
        System.out.println(" dan ac: ") ;

        int ab = input.nextInt() ;
        int bc = input.nextInt() ;
        int ac = input.nextInt() ;

        int maksimal = Math.max(ab , Math.max(ac , bc)) ;
        
        if (maksimal == ab) {
            double sisiMiring = Math.sqrt(Math.pow(ac , 2) + Math.pow(bc , 2)) ;
            if (sisiMiring == maksimal) {
                System.out.println("segitiga siku-siku, mereka penyusup") ;
                System.out.println("c pemimpin mereka") ;
            } else {
                System.out.println("bukan segitiga siku-siku, mereka bukan penyusup") ;
            } 
        } else if (maksimal == bc) {
            double sisiMiring = Math.sqrt(Math.pow(ab , 2) + Math.pow(ac , 2)) ;
            if (sisiMiring == maksimal) {
                System.out.println ("segitiga siku-siku, mereka penyusup") ;
                System.out.println("a pemimpin mereka") ;
            } else {
                System.out.println("bukan segitiga siku-siku, mereka bukan penyusup") ;
            }
        } else if (maksimal == ac) {
            double sisiMiring = Math.sqrt(Math.pow(ab , 2) + Math.pow(bc , 2)) ;
            if (sisiMiring == maksimal) {
                System.out.println("segitiga siku-siku, mereka penyusup") ;
                System.out.println("b pemimpin mereka") ;
            } else {
                System.out.println("bukan segitiga siku-siku, mereka bukan penyusup") ;
            }
        } else {
            System.out.println("bukan segitiga siku-siku, mereka bukan penyusup") ;
        }
    }

}