public class PegawaiSpesial extends Pegawai implements BisaLibur{
    public PegawaiSpesial(String nama, Double uang, String level){
        super(nama, uang, level);
    }

    public String bicara() {
        return ("Halo, saya " + getNama() + ". Uang saya adalah " + getUang() + ". Level keahlian saya adalah " + this.getLevel() + ". Dan saya bisa liburan!");
    }

    public String bisaLibur(){
        return (getNama() + " sedang berlibur ke Jerman.");
    }

    public String toString() {
        return this.bicara();
    }
}
