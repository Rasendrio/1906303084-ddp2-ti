import java.util.ArrayList;
import java.io.*;

public class Dosen extends Manusia{
    private String nip;
    private ArrayList<MataKuliah> daftarMatkul = new ArrayList<>();
    public Dosen(String nama, String nip){
        super(nama);
        this.nip = nip; 
    }
    public String getNip() {
        return this.nip;
    }
    public void setNip(String nip) {
        this.nip = nip;
    }
    public void assignMatkul(MataKuliah matkul){
        this.daftarMatkul.add(matkul);
    }
    public ArrayList<MataKuliah> getDaftarMatKul() {
        return this.daftarMatkul;
    }
}

