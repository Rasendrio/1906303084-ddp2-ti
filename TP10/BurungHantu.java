public class BurungHantu extends Hewan{
    public BurungHantu(String nama, String spesies){
        super(nama,spesies);
    }

    public String bersuara(){
        return ("Hoooh hoooh. Halo, Saya " + getNama() + ". Saya adalah Burung Hantu");
    }

    public String bergerak(){
        return (getNama() + " bergerak dengan cara terbang.");
    }

    public String bernafas(){
        return (getNama() + " bernafas dengan paru-paru.");
    }

    public String toString(){
        return this.bersuara();
    }
}
    

