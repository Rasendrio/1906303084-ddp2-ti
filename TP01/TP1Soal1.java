import java.util.Scanner;

public class TP1Soal1 {
    public static void main(String[] args) {
        
        System.out.println("Selamat Datang di DDP2") ;

        System.out.print("Masukkan Angka:  ") ;
        Scanner input = new Scanner(System.in);
        int angka = input.nextInt() ;

        if(Math.pow(angka,2) == 4){
            System.out.println("Angka bernilai 2") ;
        }else if(angka % 2 == 0){
            System.out.println("Angka tersebut genap") ;
        }else{
            System.out.println("Bukan genap dan bukan 2") ;
        }
    
        System.out.print("Masukkan Kata: ") ;
        Scanner str = new Scanner(System.in);
        String panjang = str.nextLine() ;

        int len = panjang.length() ;
        
        if(len < 5){
            System.out.println("Panjang Katanya Kurang Dari 5") ;
        }else if(len > 22){
            System.out.println("Panjang Katanya Lebih Dari 22") ;
        }else{
            System.out.println("Kata Yang Anda Masukkan Biasa Saja ") ;
        }
    } 
}
        