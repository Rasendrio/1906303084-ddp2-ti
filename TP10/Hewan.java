public abstract class Hewan implements Bernafas, Bergerak {
    private String nama;
    private String spesies;

    public Hewan(String nama, String spesies){
        this.nama = nama;
        this.spesies = spesies;

    }

    public String getNama(){
        return nama;
    }

    public String getSpesies(){
        return spesies;
    }

    public abstract String bersuara();

}
