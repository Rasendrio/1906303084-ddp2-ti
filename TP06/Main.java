import java.util.Scanner ;

public class Main {
    public static void main (String [] args){
        Scanner input = new Scanner(System.in) ;
        System.out.print("Masukkan Jumlah Karyawan: ") ;
        int jumlah = input.nextInt() ;
        Karyawan [] listKaryawan = new Karyawan[jumlah] ;
        for (int i = 0 ; i < jumlah ; i++) {
            String nama = input.next() ;
            int umur = input.nextInt() ;
            int lamaBekerja = input.nextInt() ;
            Karyawan karyawan = new Karyawan(nama, umur, lamaBekerja) ;
            listKaryawan[i] = karyawan ;
        }

        System.out.printf("Rata-rata gaji dari karyawan adalah %.2f%n", average(listKaryawan)) ;
        System.out.println("Karyawan dengan gaji terendah adalah " + minimum(listKaryawan)) ;
        System.out.println("Karyawan dengan gaji tertinggi adalah " + maximum(listKaryawan)) ;
    }

    public static double average(Karyawan[] listKaryawan) {
        double total = 0 ;
        double average = 0 ;
        for(int i = 0 ; i < listKaryawan.length ; i++) {
            total += listKaryawan[i].getGaji() ;
        }
        average = total/listKaryawan.length ;
        return average ;
    }

    public static String minimum(Karyawan[] listKaryawan) {
        Karyawan lowest = listKaryawan[0] ;
        for(int i = 0 ; i < listKaryawan.length ; i++) {
            Karyawan check = listKaryawan[i] ;
            if (check.getGaji() < lowest.getGaji()) {
                lowest = check ;
            }
        }
        return lowest.getNama() ;
    }

    public static String maximum(Karyawan[] listKaryawan) {
        Karyawan highest = listKaryawan[0] ;
        for(int i = 0 ; i < listKaryawan.length ; i++) {
            Karyawan check = listKaryawan[i] ;
            if (check.getGaji() > highest.getGaji()) {
                highest = check ;
            }
        }
        return highest.getNama() ;
    }
}