import java.util.Scanner;

public class TP1Soal2 {
    public static void main(String[] args) {
        System.out.println("Selamat datang di DDP2") ;
        
        System.out.print("Nama fakultas apa yang anda ingin ketahui?: ") ;
        Scanner str = new Scanner(System.in);
        String kata = str.nextLine() ;
        
        String fakultas = kata.toUpperCase() ;

        switch(fakultas){
            case "FK" :
                System.out.println("Fakultas Kedokteran") ;
            case "FEB" :
                System.out.println("Fakultas Ekonomi dan Bisnis") ;
                 
            case "FH" :
                System.out.println("Fakultas Hukum") ;
                 
            case "FT" :
                System.out.println("Fakultas Teknik") ;
                break ;
            case "FF" :
                System.out.println("Fakultas Farmasi") ;
                break ;
            case "FPSI" :
                System.out.println("Fakultas Psikologi") ;
                break ;
            case "FASILKOM" :
                System.out.println("Fakultas Ilmu Komputer") ;
                break ;
            case "FKG" :
                System.out.println("Fakultas Kedokteran Gigi") ;
                break ;
            case "FIA" :
                System.out.println("Fakultas Ilmu Administrasi") ;
                break ;
            case "FIK" :
                System.out.println("Fakultas Ilmu Keperawatan") ;
                break ;
            case "FKM" :
                System.out.println("Fakultas Kesehatan Masyarakat") ;
                break ;
            case "FIB" :
                System.out.println("Fakultas Ilmu Pengetahuan Budaya") ;
                break ;
            case "FISIP" :
                System.out.println("Fakultas Ilmu Sosial dan Ilmu Politik") ;
                break ;
            case "FMIPA" :
                System.out.println("Fakultas Matematika dan Ilmu Pengetahuan Alam") ;
                break ;
            default :
                System.out.println("Fakultas tidak ditemukan") ;
        }
    }

} 