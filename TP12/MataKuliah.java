import java.io.IOException;
import java.util.ArrayList;

public class MataKuliah extends Manusia {
    private String nama;
    private String kode;
    private ArrayList<Manusia> daftarMhs = new ArrayList<>();
    

    public MataKuliah(String nama, String kode){
        this.nama = nama;
        this.kode = kode;
    }

    public void tambahMhs(Manusia mhs){
        this.daftarMhs.add(mhs);
    }

    public void dropMhs(Manusia mhs){
        try{
            daftarMhs.remove(mhs);
        }catch(Exception ex) {
                System.out.println("Mahasiswa tidak terdapat dalam daftar");
        }
    }
    public void assignDosen (Manusia dosen) throws IOException{
        try{
            if {
                Dosen pengajar = new Dosen(dosen.getNama(), ((Dosen) dosen).getNip());
                pengajar.assignMatkul(this);
            }
        }catch (Exception e) {
            System.out.println("iya salah");
        }
    }
    public String tipeMatkul(String kode){
        String tipe = "";
        if (kode.substring(0,4).equals("UIGE")){
            tipe = "Mata Kuliah Wajib Universitas";
        }
        else if (kode.substring(0,4).equals("UIST")){
            tipe =  "Mata Kuliah Wajib Rumpun Sains dan Teknologi";
        }
        else if (kode.substring(0,4).equals("CSGE")){
            tipe = "Mata Kuliah Wajib Fakultas";
        }
        else if (kode.substring(0,4).equals("CSCM")){
            tipe = " Mata Kuliah Wajib Program Studi Ilmu Komputer";
        }
        else if (kode.substring(0,4).equals("CSIM")){
            tipe = "Mata Kuliah Wajib Program Studi Sistem Informasi";
        }
        else if (kode.substring(0,4).equals("CSCE")){
            tipe = "Mata Kuliah Peminatan Program Studi Ilmu Komputer";
        }
        else if (kode.substring(0,4).equals("CSIE")){
            tipe = "Mata Kuliah Peminatan Program Studi Sistem Informasi";
        }
        return tipe;
    }

    
    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getKode() {
        return kode;
    }

    public void setKode(String kode) {
        this.kode = kode;
    }

    public ArrayList<Manusia> getDaftarMhs() {
        return daftarMhs;
    }

    public void setDaftarMhs(ArrayList<Manusia> daftarMhs) {
        this.daftarMhs = daftarMhs;
    }

    public String toString(){
        return "Mata kuliah " + this.nama+" kode mahasiswa "+ this.kode;
    }
}
