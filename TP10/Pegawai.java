public class Pegawai extends Manusia{
    private String level;

    public Pegawai(String nama, Double uang, String level){
        super(nama, uang);
        this.level = level;
    }

    public String bekerja() {
        return (getNama() + " bekerja di voidMain.");
    }

    public String getLevel() {
        return level;
    }
    @Override
    public String bicara(){
        return ("Halo, saya " + getNama() + ". Uang saya adalah " + getUang() + ". Level keahlian saya adalah " + this.getLevel());
    }
}
