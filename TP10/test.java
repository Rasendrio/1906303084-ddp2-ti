import static org.junit.Assert.assertEquals;
import org.junit.Test;

public class TP10Test {
    public void testPelangganAttributes(){
        Manusia x = new Pelanggan("Acen", 10000.0);
        assertEquals("Acen", x.getNama());
        assertEquals((10000.0), x.getUang());
    }

    public void testPegawaiAttributes(){
        Pegawai x = new Pegawai("Kevin",10000.0,"Ahli");
        assertEquals("Kevin", x.getNama());
        assertEquals((10000.0), x.getUang());
        assertEquals("Ahli", x.getLevel());
    }

    public void testIkanAttributes(){
        Ikan x = new Ikan("Cupang", "Betta Splendens", false);
        assertEquals("Cupang", x.getNama());
        assertEquals("Betta Splendens", x.getSpesies());
        assertEquals(false, x.getBeracun());
    }

    public void testBurhanAttributes(){
        Hewan x = new BurungHantu("Burhan", "Bubo Scandiacus");
        assertEquals("Burhan", x.getNama());
        assertEquals("Bubo Scandiacus", x.getSpesies());
    }
}
